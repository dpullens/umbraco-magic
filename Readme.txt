---Small Intro---
Welcome to the wonderous world of Magic the Gathering!
Magic is a cardgame where you build a deck with your cards and battle other players.

---Credentials---
admin@admin.com
MTGUmbraco

---Features---
* The website is Multilingual. To change languages, for now, please go to /de
* You can search for cards (Currently three) and sort them. It is case sensitive.
* You can go to the card's page by clicking the card