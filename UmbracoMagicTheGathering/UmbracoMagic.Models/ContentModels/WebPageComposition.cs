﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.ModelsBuilder;

namespace UmbracoMagic.Models.ContentModels
{
    public partial class WebPageComposition
    {
        ///<summary>
        /// Url Segment: Custom Url name. Default is the name of the document
        ///</summary>
        [ImplementPropertyType("urlSegment")]
        public string UrlSegment
        {
            get {
                return this.GetVortoValue<string>("urlSegment");
            }
        }

        /// <summary>
        /// Gets the url segment
        /// </summary>
        /// <param name="language">The language that it needs the segment for</param>
        /// <returns>The correct url segment</returns>
        public string GetUrlSegment(string language)
        {
            string segment = this.GetVortoValue<string>("urlSegment", language);
            if (string.IsNullOrEmpty(segment))
            {
                segment = this.Name;
            }
            return segment.EnsureEndsWith("/");
        }
    }
}