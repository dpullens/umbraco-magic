﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.ModelsBuilder;

namespace UmbracoMagic.Models.ContentModels
{
    public partial class Home
    {
        ///<summary>
        /// Page Text: The text of the homepage
        ///</summary>
        [ImplementPropertyType("pageText")]
        public string PageText
        {
            get { return this.GetVortoValue<string>("pageText"); }
        }

        ///<summary>
        /// Page Title: Title of the page within the website
        ///</summary>
        [ImplementPropertyType("pageTitle")]
        public string PageTitle
        {
            get { return this.GetVortoValue<string>("pageText"); }
        }

        /// <summary>
        /// Gets the url segment
        /// </summary>
        /// <param name="language">The language that it needs the segment for</param>
        /// <returns>The correct url segment</returns>
        public string GetUrlSegment(string language)
        {
            string segment = this.GetVortoValue<string>("urlSegment", language);
            if (string.IsNullOrEmpty(segment))
            {
                segment = this.Name;
            }
            return segment.EnsureEndsWith("/");
        }
    }
}