﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.ModelsBuilder;
using Umbraco.Core;

namespace UmbracoMagic.Models.ContentModels
{
    public partial class MagicCard
    {
        ///<summary>
        /// Card Title: Title of the Card
        ///</summary>
        [ImplementPropertyType("cardTitle")]
        public string CardTitle
        {
            get {
                return this.GetVortoValue<string>("cardTitle");
            }
        }

        /// <summary>
        /// Gets the url segment
        /// </summary>
        /// <param name="language">The language that it needs the segment for</param>
        /// <returns>The correct url segment</returns>
        public string GetUrlSegment(string language)
        {
            string segment = this.GetVortoValue<string>("urlSegment", language);
            if (string.IsNullOrEmpty(segment))
            {
                segment = this.Name;
            }
            return segment.EnsureEndsWith("/");
        }

        ///<summary>
        /// Card Image: The image of the card
        ///</summary>
        [ImplementPropertyType("cardImage")]
        public Image CardImage
        {
            get
            {
                var publishedContent = this.GetPropertyValue<IPublishedContent>("cardImage");
                return publishedContent == null ? null : new Image(publishedContent);
            }
        }
    }
}