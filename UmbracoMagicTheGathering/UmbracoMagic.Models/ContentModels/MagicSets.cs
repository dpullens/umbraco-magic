﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;

namespace UmbracoMagic.Models.ContentModels
{
    public partial class MagicSets
    {
        /// <summary>
        /// Gets the url segment
        /// </summary>
        /// <param name="language">The language that it needs the segment for</param>
        /// <returns>The correct url segment</returns>
        public string GetUrlSegment(string language)
        {
            string segment = this.GetVortoValue<string>("urlSegment", language);
            if (string.IsNullOrEmpty(segment))
            {
                segment = this.Name;
            }
            return segment.EnsureEndsWith("/");
        }
    }
}