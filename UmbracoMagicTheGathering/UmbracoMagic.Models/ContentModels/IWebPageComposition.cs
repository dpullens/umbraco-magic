﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoMagic.Models.ContentModels
{
    public partial interface IWebPageComposition
    {
        string GetUrlSegment(string language);
    }
}