﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoMagic.Web.Core.ViewModels.Master
{
    /// <summary>
    /// ViewModel for the Seo view
    /// </summary>
    public class SeoViewModel
    {
        /// <summary>
        /// Gets or sets the page title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the seo meta description
        /// </summary>
        public string Description { get; set; }
    }
}