﻿using System.Collections.Generic;
using UmbracoMagic.Web.Core.ViewModels.Card;

namespace UmbracoMagic.Web.Core.ViewModels.Search
{
    public class SearchResultViewModel
    {
        public SearchResultViewModel()
        {
            Results = new List<CardViewModel>();
        }

        /// <summary>
        /// Gets or sets the results
        /// </summary>
        public IEnumerable<CardViewModel> Results { get; set; }
    }
}