﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoMagic.Web.Core.ViewModels.Homepage
{
    public class HomeViewModel
    {
        /// <summary>
        /// Gets or sets the page title
        /// </summary>
        public string PageTitle { get; set; }

        /// <summary>
        /// Gets or sets the page text
        /// </summary>
        public string PageText { get; set; }
    }
}