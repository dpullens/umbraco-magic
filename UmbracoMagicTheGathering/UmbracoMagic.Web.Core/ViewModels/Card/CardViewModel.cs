﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UmbracoMagic.Models.ContentModels;

namespace UmbracoMagic.Web.Core.ViewModels.Card
{
    public class CardViewModel
    {
        public string CardName { get; set; }

        public string CardType { get; set; }

        public string Artist { get; set; }

        public Image CardImage { get; set; }

        public string CardText { get; set; }

        public string Url { get; set; }
    }
}