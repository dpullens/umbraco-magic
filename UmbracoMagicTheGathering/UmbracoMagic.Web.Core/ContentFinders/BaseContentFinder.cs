﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using UmbracoMagic.Models.ContentModels;

//BaseContentFinder is based on https://github.com/jbreuer/1-1-multilingual-example

namespace UmbracoMagic.Web.Core.ContentFinders
{
    public class BaseContentFinder : IContentFinder
    {
        /// <summary>
        /// Tries to find the content based on the given request
        /// </summary>
        /// <param name="contentRequest">The given request</param>
        /// <returns>If the content was found</returns>
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest == null)
            {
                return false;
            }

            try
            {
                var helper = new UmbracoHelper(UmbracoContext.Current);

                var urls = (List<Tuple<int, string>>)UmbracoContext.Current.Application.ApplicationCache.RuntimeCache.GetCacheItem(
                    "MultilingualContentFinder-Urls",
                    () =>
                    {
                        var contentUrls = new List<Tuple<int, string>>();

                        // Get all the nodes in the website.
                        var allNodes = helper.TypedContentAtRoot().DescendantsOrSelf<IPublishedContent>().ToList();

                        foreach (var node in allNodes)
                        {
                            // Get all the urls in the website.
                            // With UrlProvider.GetOtherUrls we also get the urls of the other languages.
                            contentUrls.Add(new Tuple<int, string>(node.Id, node.Url));
                            contentUrls.AddRange(UmbracoContext.Current.UrlProvider.GetOtherUrls(node.Id).Select(x => new Tuple<int, string>(node.Id, x)));
                        }

                        return contentUrls;
                    });

                if (urls.Any())
                {
                    // Get the current url without querystring.
                    var url = this.RemoveQueryFromUrl(contentRequest.Uri.ToString()).EnsureEndsWith("/");

                    var currentUrlItem = urls.FirstOrDefault(x => url.InvariantEquals(x.Item2));

                    if (currentUrlItem != null)
                    {
                        var contentItem = UmbracoContext.Current.ContentCache.GetById(currentUrlItem.Item1);

                        if (contentItem != null)
                        {
                            contentRequest.PublishedContent = contentItem;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<BaseContentFinder>("Error in contenfinder MultilingualContentFinder", ex);
            }

            return contentRequest.PublishedContent != null;
        }

        /// <summary>
        /// Remove the querystring from a url.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string RemoveQueryFromUrl(string url)
        {
            var index = url.IndexOf('?');

            return index > 0 ? url.Substring(0, index) : url;
        }
    }
}