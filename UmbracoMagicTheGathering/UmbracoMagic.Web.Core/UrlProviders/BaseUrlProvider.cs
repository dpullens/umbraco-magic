﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using UmbracoMagic.Models.ContentModels;

//BaseUrlProvider is based on https://github.com/jbreuer/1-1-multilingual-example

namespace UmbracoMagic.Web.Core.UrlProviders
{
    public class BaseUrlProvider : IUrlProvider
    {
        /// <summary>
        /// Gets the urls that aren't of the first language
        /// </summary>
        /// <param name="umbracoContext">The umbraco context</param>
        /// <param name="id">The id of the content</param>
        /// <param name="current">The current URI</param>
        /// <returns>A list of urls that belong to the current content, in all other languages than default</returns>
        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            return (IEnumerable<string>)UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem(
                "MultilingualUrlProvider-GetOtherUrls-" + id,
                () =>
                {
                    var content = umbracoContext.ContentCache.GetById(id) as IWebPageComposition;

                    if (content != null)
                    {
                        var domains = ApplicationContext.Current.Services.DomainService.GetAll(true).OrderBy(x => x.Id).ToList();
                        if (domains.Count > 1)
                        {
                            var urls = new List<string>();

                            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
                            var currentCulture = Thread.CurrentThread.CurrentCulture.ToString();

                            // Find the domain that's used in the GetUrl method.
                            var domain = UmbracoContext.Current.IsFrontEndUmbracoRequest
                                             ? domains.First(x => x.LanguageIsoCode.InvariantEquals(currentCulture))
                                             : domains.First();

                            // Remove the found domain because it's already used.
                            domains.Remove(domain);

                            if (content.DocumentTypeAlias.InvariantEquals(Home.ModelTypeAlias))
                            {
                                // Return the domain if we're on the homepage because on that node we've added the domains.
                                urls.AddRange(domains.Select(x => x.DomainName.EnsureEndsWith("/")));
                            }
                            else
                            {
                                // Get the other urls for the parent which aren't the main url.
                                var parentUrls = umbracoContext.UrlProvider.GetOtherUrls(content.Parent.Id).ToList();

                                for (int i = 0; i < domains.Count; i++)
                                {
                                    // Get the domain and matching parent url.
                                    var otherDomain = domains[i];
                                    var otherParentUrl = parentUrls[i];

                                    // Get the parent url and add the url segment of this culture.
                                    var urlSegment = content.GetUrlSegment(otherDomain.LanguageIsoCode);
                                    urls.Add(otherParentUrl.EnsureEndsWith("/") + urlSegment);
                                }
                            }

                            return urls;
                        }
                    }

                    return null;
                });
        }

        /// <summary>
        /// Gets the url for the given content in the default language
        /// </summary>
        /// <param name="umbracoContext">The Umbraco Context</param>
        /// <param name="id">The id of the content</param>
        /// <param name="current">The current Uri</param>
        /// <param name="mode">The UrlProvider Mode</param>
        /// <returns>The url of the content for the default language</returns>
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            return (string)UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem(
                "MultilingualUrlProvider-GetUrl-" + id,
                () =>
                {
                    var content = umbracoContext.ContentCache.GetById(id) as IWebPageComposition;

                    if (content != null)
                    {
                        var domains =
                            ApplicationContext.Current.Services.DomainService.GetAll(true)
                                .OrderBy(x => x.Id)
                                .ToList();

                        if (domains.Any())
                        {
                            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
                            var currentCulture = Thread.CurrentThread.CurrentCulture.ToString();

                            // On the frontend get the domain that matches the current culture. We could also check the domain name, but for now the culture is enough.
                            // Otherwise just get the first domain. The urls for the other domains are generated in the GetOtherUrls method.
                            var domain = UmbracoContext.Current.IsFrontEndUmbracoRequest
                                             ? domains.First(x => x.LanguageIsoCode.InvariantEquals(currentCulture))
                                             : domains.First();

                            if (content.DocumentTypeAlias.InvariantEquals(Home.ModelTypeAlias))
                            {
                                // Return the domain if we're on the homepage because on that node we've added the domains.
                                return domain.DomainName.EnsureEndsWith("/");
                            }

                            // Get the parent url and add the url segment of this culture.
                            var parentUrl = umbracoContext.UrlProvider.GetUrl(content.Parent.Id);
                            var urlSegment = content.GetUrlSegment(domain.LanguageIsoCode);
                            return parentUrl.EnsureEndsWith("/") + urlSegment;
                        }
                    }

                    return null;
                });
        }
    }
}