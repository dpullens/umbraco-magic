﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Models.ContentModels;
using UmbracoMagic.Web.Core.ViewModels.Card;

namespace UmbracoMagic.Web.Core.Mappings.Card
{
    public class IPublishedContentToCardViewModelConverter : ITypeConverter<IPublishedContent, CardViewModel>
    {
        public CardViewModel Convert(ResolutionContext context)
        {
            var destination = new CardViewModel();

            if (context.IsSourceValueNull)
            {
                return destination;
            }

            var source = context.SourceValue as IPublishedContent;

            if (source == null)
            {
                return destination;
            }

            MagicCard card = source as MagicCard;

            if (card == null)
            {
                return destination;
            }

            destination.CardName = card.CardTitle;
            destination.CardType = card.CardType;
            destination.Artist = card.CardArtist;
            destination.CardImage = card.CardImage;
            destination.CardText = card.CardText;
            destination.CardType = card.CardType;
            destination.Url = card.Url;

            return destination;
        }
    }
}