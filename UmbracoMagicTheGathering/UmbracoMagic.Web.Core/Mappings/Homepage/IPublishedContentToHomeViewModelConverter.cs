﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Models.ContentModels;
using UmbracoMagic.Web.Core.ViewModels.Homepage;

namespace UmbracoMagic.Web.Core.Mappings.Homepage
{
    public class IPublishedContentToHomeViewModelConverter : ITypeConverter<IPublishedContent, HomeViewModel>
    {
        public HomeViewModel Convert(ResolutionContext context)
        {
            var destination = new HomeViewModel();

            if (context.IsSourceValueNull)
            {
                return destination;
            }

            var source = context.SourceValue as IPublishedContent;

            if (source == null)
            {
                return destination;
            }

            Home homepage = source as Home;

            if (homepage == null)
            {
                return destination;
            }

            destination.PageTitle = homepage.PageTitle;
            destination.PageText = homepage.PageText;

            return destination;
        }
    }
}