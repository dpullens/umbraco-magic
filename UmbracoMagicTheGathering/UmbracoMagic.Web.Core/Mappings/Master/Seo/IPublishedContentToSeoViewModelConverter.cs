﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Models.ContentModels;
using UmbracoMagic.Web.Core.ViewModels.Master;

namespace UmbracoMagic.Web.Core.Mappings.Master.Seo
{
    /// <summary>
    /// Converts IPublishedContent to a SeoViewModel
    /// </summary>
    public class IPublishedContentToSeoViewModelConverter : ITypeConverter<IPublishedContent, SeoViewModel>
    {
        /// <summary>
        /// Tries to create a SeoViewModel from the IPublishedContent
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public SeoViewModel Convert(ResolutionContext context)
        {
            var destination = new SeoViewModel();

            if(context.IsSourceValueNull)
            {
                return destination;
            }

            var source = context.SourceValue as IPublishedContent;

            if(source == null)
            {
                return destination;
            }

            ISeoComposition seo = source as ISeoComposition;

            if(seo == null)
            {
                return destination;
            }

            destination.Title = seo.BrowserTitle;
            destination.Description = seo.MetaDescription;

            return destination;
        }
    }
}