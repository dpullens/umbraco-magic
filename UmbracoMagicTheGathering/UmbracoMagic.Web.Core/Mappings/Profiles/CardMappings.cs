﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Web.Core.Mappings.Card;
using UmbracoMagic.Web.Core.ViewModels.Card;

namespace UmbracoMagic.Web.Core.Mappings.Profiles
{
    public class CardMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IPublishedContent, CardViewModel>().ConvertUsing<IPublishedContentToCardViewModelConverter>();
        }
    }
}