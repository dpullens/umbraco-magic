﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Web.Core.Mappings.Master.Seo;
using UmbracoMagic.Web.Core.ViewModels.Master;

namespace UmbracoMagic.Web.Core.Mappings.Profiles
{
    public class SeoMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IPublishedContent, SeoViewModel>().ConvertUsing<IPublishedContentToSeoViewModelConverter>();
        }
    }
}