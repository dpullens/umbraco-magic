﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoMagic.Web.Core.Mappings.Homepage;
using UmbracoMagic.Web.Core.ViewModels.Homepage;

namespace UmbracoMagic.Web.Core.Mappings.Profiles
{
    public class HomepageMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IPublishedContent, HomeViewModel>().ConvertUsing<IPublishedContentToHomeViewModelConverter>();
        }
    }
}