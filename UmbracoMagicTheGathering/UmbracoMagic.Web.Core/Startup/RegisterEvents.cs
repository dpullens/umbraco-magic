﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Web.Routing;
using UmbracoMagic.Web.Core.ContentFinders;
using UmbracoMagic.Web.Core.UrlProviders;

namespace UmbracoMagic.Web.Core.Startup
{
    public class RegisterEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Register our url provider so that it hits before the default provider
            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, BaseUrlProvider>();

            //Remove the default, since our BaseUrlProvider should cover everything
            UrlProviderResolver.Current.RemoveType<DefaultUrlProvider>();

            //Register our content finder so that we can find our content base on our custom urls
            ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNiceUrl, BaseContentFinder>();

            //Remove the default, since our BaseContentFinder should cover everything
            ContentFinderResolver.Current.RemoveType<ContentFinderByNiceUrl>();
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Set the mappings we have by iterating over all the classes that inherit from Profile and adding them to the mapper
            foreach(var profile in PluginManager.Current.ResolveTypes<Profile>())
            {
                Mapper.AddProfile((Profile)Activator.CreateInstance(profile));
            }
        }
    }
}