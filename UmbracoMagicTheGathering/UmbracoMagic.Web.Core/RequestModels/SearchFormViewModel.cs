﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoMagic.Web.Core.RequestModels
{
    public class SearchFormViewModel
    {
        /// <summary>
        /// The term searched on
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// What should be ordered on
        /// </summary>
        public OrderByOptions OrderBy { get; set; }

        /// <summary>
        /// Asc or desc search
        /// </summary>
        public OrderOptions AscOrDesc { get; set; }
    }

    public enum OrderByOptions
    {
        CardName,
        Artist
    }

    public enum OrderOptions
    {
        Ascending,
        Descending
    }
}