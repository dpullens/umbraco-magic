﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmbracoMagic.Web.Core.RequestModels;
using UmbracoMagic.Web.Core.ViewModels.Homepage;
using Umbraco.Core.Models;
using UmbracoMagic.Models.ContentModels;
using UmbracoMagic.Web.Core.ViewModels.Search;
using UmbracoMagic.Web.Core.ViewModels.Card;

namespace UmbracoMagic.Web.Core.Controllers
{
    public class HomeController : SurfaceController
    {
        /// <summary>
        /// Renders the homepage content
        /// </summary>
        /// <returns>The rendering in form of an ActionResult</returns>
        [ChildActionOnly]
        public ActionResult Content()
        {
            var currentPage = UmbracoContext.PublishedContentRequest.PublishedContent;

            var viewModel = AutoMapper.Mapper.Map<HomeViewModel>(currentPage);

            return this.PartialView("Homepage/Content", viewModel);
        }

        /// <summary>
        /// Renders the search bar
        /// </summary>
        /// <returns>Actionresult for the search</returns>
        public ActionResult Search()
        {
            var model = new SearchFormViewModel();
            return this.PartialView("Homepage/Search", model);
        }

        /// <summary>
        /// Post of the search
        /// </summary>
        /// <returns>Search results in the form of an ActionResult</returns>
        public ActionResult SearchSubmit(SearchFormViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return new RedirectResult(this.Request.RawUrl);
            }

            //Current search would do better in something like Azure Search
            var results = SearchCardName(model.SearchTerm);
            results = SortResults(results, model);

            var viewModel = new SearchResultViewModel();
            viewModel.Results = results.Select(x => AutoMapper.Mapper.Map<CardViewModel>(x));

            return this.PartialView("Homepage/SearchResults", viewModel);
        }

        /// <summary>
        /// Sorts the search results depending on the parameters
        /// </summary>
        /// <param name="results">The results</param>
        /// <param name="model">The model</param>
        private IEnumerable<MagicCard> SortResults(IEnumerable<MagicCard> results, SearchFormViewModel model)
        {
            var sortedResult = results;
            switch (model.OrderBy)
            {
                case OrderByOptions.Artist:
                    {
                        if(model.AscOrDesc == OrderOptions.Ascending)
                        {
                            sortedResult = results.OrderBy(x => x.CardArtist);
                        }
                        else
                        {
                            sortedResult = results.OrderByDescending(x => x.CardArtist);
                        }
                    }
                    break;
                case OrderByOptions.CardName:
                    {
                        if (model.AscOrDesc == OrderOptions.Ascending)
                        {
                            sortedResult = results.OrderBy(x => x.CardTitle);
                        }
                        else
                        {
                            sortedResult = results.OrderByDescending(x => x.CardTitle);
                        }
                    }
                    break;
            }
            return sortedResult;
        }

        /// <summary>
        /// Searches Umbraco for the given card name
        /// </summary>
        /// <param name="cardName"></param>
        /// <returns></returns>
        private IEnumerable<MagicCard> SearchCardName(string cardName)
        {
            var searchResult = new List<MagicCard>();

            var xpathQuery = $"//magicCard[@isDoc and contains(@nodeName,'{cardName}')]";
            var results = UmbracoContext.ContentCache.GetXPathNavigator().Select(xpathQuery).Cast<XPathNavigator>();
            foreach (XPathNavigator result in results)
            {
                int id;
                if (!int.TryParse(result.GetAttribute("id", ""), out id))
                {
                    continue;
                }
                var content = UmbracoContext.ContentCache.GetById(id);

                if (content == null || !(content is MagicCard)) continue;

                var card = new MagicCard(content);

                searchResult.Add(card);
            }

            return searchResult;
        }
    }
}