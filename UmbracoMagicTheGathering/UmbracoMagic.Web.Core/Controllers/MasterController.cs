﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmbracoMagic.Web.Core.ViewModels.Master;

namespace UmbracoMagic.Web.Core.Controllers
{
    /// <summary>
    /// The controller for the master page (basically every page)
    /// </summary>
    public class MasterController : SurfaceController
    {
        public ActionResult Index()
        {
            return Content("");
        }

        /// <summary>
        /// Gets the Seo values for the current page
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult Seo()
        {
            var currentPage = UmbracoContext.PublishedContentRequest.PublishedContent;
            
            var model = AutoMapper.Mapper.Map<SeoViewModel>(currentPage);

            return this.PartialView("Seo", model);
        }

        /// <summary>
        /// Adds the css includes to the masterpage
        /// </summary>
        /// <returns></returns>
        public ActionResult Css()
        {
            return this.PartialView("Css");
        }
    }
}