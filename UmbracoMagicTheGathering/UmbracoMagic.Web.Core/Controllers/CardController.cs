﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmbracoMagic.Web.Core.ViewModels.Card;

namespace UmbracoMagic.Web.Core.Controllers
{
    public class CardController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Content()
        {
            var currentPage = UmbracoContext.PublishedContentRequest.PublishedContent;

            var model = AutoMapper.Mapper.Map<CardViewModel>(currentPage);

            return this.PartialView("Card", model);
        }
    }
}